console.log("Hello World!")


// Assignments Operator (=)

let assignmentNumber = 8;


// Addition Assignment Operator (+=)


assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); 


assignmentNumber += 2;
console.log(assignmentNumber);

assignmentNumber *= 3;
console.log(assignmentNumber)

assignmentNumber /= 6;
console.log(assignmentNumber)

// subtraction/mULTIPICATION/DIVISION/ assignment operator(-=, *=, /=)

assignmentNumber -= 2;
assignmentNumber *= 2;
assignmentNumber /= 2;

// Arithmetic Operator (+, -, *, /, %)

let mdas = 1 + 2 - 3 * 4 / 5;

console.log("result of mdas operator" + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);


// Increments and decrement 
let z = 1;
// Pre-fix Incrementation
++z
console.log(z); //2
// Post-fix Incrementation
// return the previous Value of the variable and add 1 to its actual valuer
z++
console.log(z); //3

console.log(z++); //3
console.log(z); //4

console.log(++z); //5 - the new value is returned immediately

// pre-fix Decrementation and post-fix decrementation
console.log(--z); //4
console.log(z--); //4
console.log(z);

// type Coercion

let numA = '10';
let numB = 12;

let Coercion = numA + numB;
console.log(Coercion);
console.log(typeof Coercion);


let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);

let numE = true + 1;
console.log(numE);
console.log(typeof numE);
// the result is number
// The boolean "true" is associated with the value of 1

let numF = false + 1;
console.log(numF);

// Comparison Operator
// (==) Equality Operator
console.log("Equality Operator");
console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == '1'); // true
console.log(0 == false); // true
console.log('juan' == 'JUAN'); // false -case sensetive
console.log('juan' == 'juan') //true




// (==) Strictly Operator
console.log("Strictly Equality Operator");
console.log(1 === 1); // true
console.log(1 === 2); // false
console.log(1 === '1'); // false - data type
console.log(0 === false); // false - number and a boolean
console.log('juan' === 'JUAN'); // false -case sensetive
console.log('juan' === 'juan'); //true

//(!=) Inequality Operator
console.log("InEquality Operator");
console.log(1 != 1); // false
console.log(1 != 2); // True
console.log(1 != '1'); // false
console.log(0 != false); // false
console.log('juan' != 'JUAN'); // true -case sensetive
console.log('juan' != 'juan');



//(!=) strict Inequality Operator
console.log("Strictly InEquality Operator");
console.log(1 !== 1); // false
console.log(1 !== 2); // True
console.log(1 !== '1'); // True
console.log(0 !== false); // true
console.log('juan' != 'JUAN'); // true -case sensetive

// Relational Comparison Operator

let x = 500;
let y = 700;
let w = 8000;

let numString = "5500";
console.log("Greater than")
console.log(x > y); //false

// less than
console.log("Less than")
console.log(y <= y); //false
console.log(numString < 6000); //true
console.log(numString < 1000); //false

//Greater than of Equal to

console.log("Greater than or Equal to");
console.log(w >= w); //true
console.log(y >= y); //true

// logical operator (&&, ||, !)

let isAdmin = false;
let isRegister = true;
let isLegalAge = true;

// Logical AND Operator (&& - Double Ampersand)
console.log("Logical AND Operator")
let authorization = isAdmin && isRegister;
console.log(authorization); //false

let authorization2 = isLegalAge && isRegister;
console.log(authorization2); //true

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegister && requiredLevel ===25;
console.log(authorization3); //false

// logical OR Operator (|| - Double pipe)
// return true if atleast One of the operands are true

let userLevel = 100;
let userLevel2 = 65;
let userAge = 15;

let guildRequirment = isRegister || userLevel2 >= requiredLevel || userAge >= requiredAge;

console.log(guildRequirment);

// Not Operator
console.log("Not operator");
// turns a boolean in to the opposite value

let opposite = !isAdmin;
console.log(opposite);
console.log(!isRegister);

// CONDITIONAL STATEMENTS
// if else if and if statement

// if statement
if(true) {
	console.log("We just run an if Condition");
}

let numG = 5;

if (numG < 10) {
	console.log('Hello');
}

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20

if(userName3.length > 10 && isRegister && isAdmin) {
	console.log("Welcome to Game Online")
} else{
	console.log("Your are not Ready")
}


function login(username, password){
	if(typeof username === "string" && typeof password === "string")
	console.log("both are strings")
	
	if(username.length >= 8 && password.length >= 8) {
		console.log("Thank you for loggin")
	}
	else if (username.length <=7) {
		console.log("username too short")
	}
	else if (password.length <=7) {
		console.log("password too short")
	}
	else {
		("Credentilas too short")
	}
	
}

login("janepe","pename")


 if (true) {
 	console.log("We")
 }

 let numH = 12;

 if(numH <10) {
 	console.log("Yamete")
 }
 else if(numH >10){
 	console.log("onichan")
 }




const isadmin = true;
const isRegistered = true;
const islegalAge = true;

let username5 = "HowellFLordeliz";

 if(username5.length > 5 && isRegistered && isadmin) {
	console.log("Welcome to Game Online!")
} else {
	console.log("You are not ready")
}


let n = 3;
console.log(typeof n);

function addNum(num1, num2) {
	//check if the numbers being passed as an argument are number types
	if(typeof num1 === 3 && typeof num2 === 4){
		console.log("Run only if both arguments passed are number types");
		console.log(num1 + num2)
	} else {
		console.log("One or both of the arguments are not numbers")
	}
};

addNum(5, false);



function login(username, password) {
	if(typeof username === "string" && typeof password === "string") {
		console.log("Both Arguments are string");

		if(username.length > 8 && password.length >8){
			console.log("Your login")
		}
		else if(username.length < 8 && password.length > 8){
			console.log("username too short")
		} 
		else if(password.length < 8 && username.length > 8){
			console.log("password too short")
		}
		else {
			console.log("Credentials too short")
		}
	}
	else{
		console.log("One or both of the arguments are not string");
	}	

}

login("asd","psss");



function determineTyphoneIntensity(windspeed) {
	if(windspeed <30) {
		return ('Not a typhone yet')
	}
	else if(windspeed <=61){
		return('Tropical Depression Typhone')
	}
	else if (windspeed >=61 && windspeed <=81){
		return('Severe tropical typhone')
	}
	else if(windspeed >=81 && windspeed <=101){
		return('Super Typhone')
	}
	else {
		return 'typhone detected'
	}
}

message = determineTyphoneIntensity(90);
 console.log (message);

 if(message == 'Super Typhone') {
 	console.warn(message)
 }

// Truthy and Falsy

if (0) {
	console.log("truthy")
}

let fName = "Howell";
let mName = "Mendoza";
let lName = "Flordeliz"

console.log(fName + " " + mName + " " + lName)
console.log(`${fName} ${mName} ${lName}`)

let profile =(fName + " " + mName + " " + lName)

console.log(profile)


//Single Statement execution

let ternaryResult = (1 > 18) ? true : false;
console.log(ternaryResult)


5000 > 1000 ? console.log("Price is less than 1000") : console.log("Price is over 1000")


let name;

function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal age limit'
}

function isUnderAge() {
	name = 'Jane';
	return 'You are under the age limit'
}

let age = parseInt(prompt("What is your age?"));
console.log(age)

let legalAge = (age > 18) ? isUnderAge() : isOfLegalAge();
console.log(`Result of the ternary operator in functions: ${legalAge}, ${name}`);

let day = prompt("What day of the week is Today").toLowerCase();
console.log(day);

switch(day) {
	case `monday`:
	      console.log("The color of the day is red");
	      break;
	
	case `tuesday`:
	       console.log("The color of the day is yellow");
	       break;

	case `wednesday`:
	       console.log("The color of the day is blue");
	       break;

	case `thursday`:
	       console.log("The color of the day is green");
	       break;

	case `friday`:
	      console.log("The color of the day is indigo");
	      break;

	case `saturday`:
	        console.log("The color of the day is orange");
	        break;

	case `sunday`:
	      console.log("The color of the day is violet");
	      break;

	      default:
	      	console.log("Please input a valid day")
	     
}

// try-cath finally statement

function showIntensityAlert(windspeed) {
	try{
		// attempt to execute a code
		alert(determineTyphoneIntensity(windspeed));
	}
	catch(error){
		console.log(typeof error);

		console.log(error.message)
	}
	finally {
		// continue to execute acode regardless of success or failure of acode execution
		alert(`Intensity updates will show new alert`);
	}
}

showIntensityAlert(61);



